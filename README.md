# Hexon

## 🎉 New Name

winwin-hexo-editor has been renamed to Hexon witch means hexo online. Wish you love it!

## 📘 Guide

### Before Install

Make sure your `git`, `hexo` and `Node.js` workflow is fine. Hexon only provide a GUI for these commands, but **not implement them**.

### Install

```bash
git clone https://github.com/gethexon/hexon
pnpm install
pnpm run setup
```

### Uninstall

```bash
rm -rf hexon
# just remove the folder you just cloned
```

### Start

```bash
pnpm start # for plain nodejs
pnpm prd # for pm2
```

## Commands

- `pnpm run setup`: install and config
- `pnpm start`: start hexon with node
- `pnpm prd`: start hexon with pm2
- `pnpm resetpwd [new-password]`: reset password to `[new password]`
- `pnpm script`: manage custom script

## 🖥️ Develop

- Check out `develop` branch.
- Run `pnpm dev-init` install dependencies and config hexon.
- Run `pnpm dev` and show your magic!

## ❓ Want to know more?

Start a [discussion](https://github.com/gethexon/hexon/discussions) or join us via QQ group 590355610.

## ⏱️ Looking for old version?

Versions under v0.6.x are still available at [winwin-hexo-editor](https://github.com/YuJianghao/winwin-hexo-editor/)

## License

GPL-3.0 © winwin2011
